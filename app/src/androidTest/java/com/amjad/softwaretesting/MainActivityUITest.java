package com.amjad.softwaretesting;


import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

import android.view.View;

import androidx.test.espresso.ViewInteraction;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import com.amjad.softwaretesting.utils.SuffixCalculator;

import org.hamcrest.core.IsInstanceOf;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class MainActivityUITest {

    ViewInteraction editText1, editText2, output, button;
    String testCase = "Testing";
    Integer pos1 = testCase.length() - 2;
    Integer pos2 = testCase.length() + 2;

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Before
    public void setup() {
        editText1 = onView(withId(R.id.input1));
        editText2 = onView(withId(R.id.input2));
        output = onView(withId(R.id.out));
        button = onView(withId(R.id.button));
    }

    @Test
    public void mainActivityUITest() throws IOException {
        editText1.perform(replaceText(testCase), closeSoftKeyboard());
        editText2.perform(replaceText(pos1.toString()), closeSoftKeyboard());

        button.perform(click());

        SuffixCalculator suffixCalculator = new SuffixCalculator(pos1, testCase);

        output.check(matches(withText(suffixCalculator.getSuffix())));
    }

    @Test(expected = Exception.class)
    public void mainActivityUITestThrowsException() throws IOException {
        editText1.perform(replaceText(testCase), closeSoftKeyboard());
        editText2.perform(replaceText(pos2.toString()), closeSoftKeyboard());

        button.perform(click());

        SuffixCalculator suffixCalculator = new SuffixCalculator(pos2, testCase);

        output.check(matches(withText(suffixCalculator.getSuffix())));
    }
}
