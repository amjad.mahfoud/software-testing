package com.amjad.softwaretesting;


import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.RootMatchers.withDecorView;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

import androidx.test.espresso.ViewInteraction;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class SystemTest {

    ViewInteraction editText1, editText2, output, button;
    String testCase = "Testing";
    Integer pos1 = testCase.length() - 2;
    Integer pos2 = testCase.length() + 2;

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Before
    public void setup() {
        editText1 = onView(withId(R.id.input1));
        editText2 = onView(withId(R.id.input2));
        output = onView(withId(R.id.out));
        button = onView(withId(R.id.button));
    }

    @Test
    public void mainActivitySystemTest() {
        editText1 = onView(withId(R.id.input1));
        editText2 = onView(withId(R.id.input2));
        output = onView(withId(R.id.out));

        button = onView(withId(R.id.button));

        editText1.perform(replaceText(testCase), closeSoftKeyboard());
        editText2.perform(replaceText(pos1.toString()), closeSoftKeyboard());

        button.perform(click());

        output.check(matches(withText("ng")));
    }

    @Test
    public void mainActivitySystemTestExceptionHandling() {
        editText1 = onView(withId(R.id.input1));
        editText2 = onView(withId(R.id.input2));
        output = onView(withId(R.id.out));

        button = onView(withId(R.id.button));

        editText1.perform(replaceText(testCase), closeSoftKeyboard());
        editText2.perform(replaceText(pos2.toString()), closeSoftKeyboard());

        button.perform(click());

        output.check(matches(withText("")));
        onView(withText("Length exception")).inRoot(withDecorView(not(is(getActivity().getWindow().getDecorView())))).check(matches(isDisplayed()));

    }
}
