package com.amjad.softwaretesting;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.amjad.softwaretesting.utils.SuffixCalculator;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = findViewById(R.id.button);

        EditText editText1 = findViewById(R.id.input1);
        EditText editText2 = findViewById(R.id.input2);

        TextView output = findViewById(R.id.out);

        button.setOnClickListener(v -> {
            try {
                output.setText(
                        new SuffixCalculator(
                                Integer.parseInt(editText2.getText().toString()),
                                editText1.getText().toString()
                        ).getSuffix());
            } catch (Exception e) {
                Toast.makeText(
                        getApplicationContext(),
                        e.getMessage(),
                        Toast.LENGTH_LONG
                ).show();
            }
        });
    }
}