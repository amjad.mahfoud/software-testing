package com.amjad.softwaretesting.utils;

import java.io.IOException;

public class SuffixCalculator {

    private int startingPosition;
    private String sentence;

    public SuffixCalculator(int startingPosition, String sentence) {
        this.startingPosition = startingPosition;
        this.sentence = sentence;
    }


    public String getSuffix() throws IOException {
        if (this.startingPosition > this.sentence.length())
            throw new IOException("Length exception");
        return this.sentence.substring(this.startingPosition);
    }
}
