package com.amjad.softwaretesting.utils;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class SuffixCalculatorTest {

    SuffixCalculator suffixCalculator;
    String testCase = "testing";
    int pos1 = testCase.length() - 2;
    int pos2 = testCase.length() + 2;

    @Test
    public void getSuffix() throws IOException {
        suffixCalculator = new SuffixCalculator(pos1, testCase);
        try {
            assertEquals("ng", suffixCalculator.getSuffix());
        } catch (Exception e) {
            fail();
        }
    }

    @Test(expected = Exception.class)
    public void getSuffixThrowException() throws IOException {
        suffixCalculator = new SuffixCalculator(pos2, testCase);
        assertEquals("ng", suffixCalculator.getSuffix());
    }
}